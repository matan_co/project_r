#first we need to initialize and load libraries needed for the analysis
library(data.table)
library(dplyr)
library(stringr)
library(ngram)
library(tokenizers)
library(text2vec)
library(tidyr)
library(tm)
library(NLP)
library(LiblineaR)
library(Matrix)
library(xgboost)
library(pROC)
library(caret)
library(text2vec)
library(SparseM)

# read the data
data <- read.csv('Data/train.csv', stringsAsFactors = FALSE)

# split the data set on training and testing sets (80% - training; 20% - testing)
train <- head(data, round(nrow(data)*0.8))
test <- tail(data, round(nrow(data)*0.2))

train <- train %>% mutate(filter="train")
test <- test %>% mutate(filter="test")

tot_comments <- train %>% bind_rows(test)
nrow(tot_comments)


data_preprocessing <- function(data){
  
  data <- tolower(data)
  
  # first we need to remove links from text comments
  data <- gsub("(f|ht)tp(s?)://\\S+", "LINK", data, perl = T)
  data <- gsub("http\\S+", "LINK", data, perl = T)
  data <- gsub("xml\\S+", "LINK", data, perl = T)
  
  # after we convert multiple whitspace to one
  data <- gsub("\\s+", " ", data, perl = T)

  # based on initial search through the data we need to
  # convert short word forms to the appropriate format
  data <- gsub("'ll", " will", data, perl = T)
  data <- gsub("i'm", "i am", data, perl = T)
  data <- gsub("'re", " are", data, perl = T)
  data <- gsub("'s", " is", data, perl = T)
  data <- gsub("'ve", " have", data, perl = T)
  data <- gsub("'d", " would", data, perl = T)
  data <- gsub("can't", "can not", data, perl = T)
  data <- gsub("don't", "do not", data, perl = T)
  data <- gsub("doesn't", "does not", data, perl = T)
  data <- gsub("isn't", "is not", data, perl = T)
  data <- gsub("aren't", "are not", data, perl = T)
  data <- gsub("couldn't", "could not", data, perl = T)
  data <- gsub("mustn't", "must not", data, perl = T)
  data <- gsub("didn't", "did not", data, perl = T)
  
  # now we define a list with words that can be missleading during the analysis
  # after we remove them from comments
  removewords <- c("put", "far", "bit", "well", "still", "much", "one", "two", "don", "now", "even", 
                      "uh", "oh", "also", "get", "just", "hi", "hello", "ok", "dont", "hey", "however", "id", 
                      "yeah", "yo", "wiki", "know", "talk", "read", "time", "sentence", 
                      "ain't", "wow", "org", "com", "en", "ip", "ip address", "http", "www", "html", "htm",
                      "wikimedia", "https", "httpimg", "url", "urls", "utc", "uhm",
                       "he", "him", "his", "himself", "she", "her", "hers", "herself", 
                      "it", "its", "itself", "they", "them", "their", "theirs", "themselves",
                      "what", "which", "who", "whom", "this", "that", "these", "those",
                      "is", "was", "were", "have", "has", "had", "having", "wasn't", "weren't", "hasn't",
                      "haven't", "hadn't", "won't", "wouldn't",  "do", "does", "did", "doing", "would", 
                      "should", "could",  "be", "been", "being", "ought", "shan't", "shouldn't", "let's", 
                      "that's", "who's", "what's", "here's",  "there's", "when's", "where's", "why's", 
                      "how's", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", 
                      "while", "of", "at", "by", "for", "with", "about", "against",
                      "between", "into", "through", "during", "before", "after", "above", "below", "to", 
                      "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", "further", 
                      "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", 
                      "each", "few", "more", "most", "other", "some", "such", "no", "nor", "only", "own", 
                      "same", "so", "than", "too", "very")

  data <- removeWords(data, removewords)
  return(unname(data))
}

# clean train dataset
for(i in 1:nrow(train)){
  train$comment_text[i] <- data_preprocessing(train$comment_text[i])
}

# clean test dataset
for(i in 1:nrow(test)){
  test$comment_text[i] <- data_preprocessing(test$comment_text[i])
}

# tokenize train set
tok_train = itoken(train$comment_text, 
                  preprocessor = tolower, 
                  tokenizer = word_tokenizer, 
                  ids = train$id, 
                  progressbar = FALSE)

dtm_train  <- create_dtm(tok_train, hash_vectorizer(2 ^ 20, c(1L, 1L)))
dtm_train  <- (dtm_train > 0) * 1

summary(dtm_train)

# tokenize test set
tok_test = itoken(test$comment_text, 
                 preprocessor = tolower, 
                 tokenizer = word_tokenizer, 
                 ids = test$id, 
                 progressbar = FALSE)


dtm_test <- create_dtm(tok_test, hash_vectorizer(2 ^ 20, c(1L, 1L)))
dtm_test  <- (dtm_test > 0) * 1

target_vector <- c('toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate')

# training xgboost
train_xgb <- function(target_vector){
  
  train_parameters <- list(booster = "gbtree",
                           eta = 0.05,
                           gamma = 0,
                           max_depth = 3,
                           lambda = 0.01,
                           alpha = 0.5,
                           objective = "binary:logistic")
  
  data <- xgb.DMatrix(data = dtm_train, label = as.matrix(train[ , target_vector]))
  
  model_xgb <- xgb.train(data = data, 
                         params = train_parameters,
                         print_every_n = 10,
                         eval_metric = "auc", nrounds = 500)
  
  preds <- predict(model_xgb, dtm_test)
  test_p <- cbind(c(), preds)
  return(test_p)
  
}

done_xgb <- lapply(target_vector, train_xgb)

prediction_xgb <- data.frame(id = test$id,
                             toxic = done_xgb[[1]], 
                             severe_toxic = done_xgb[[2]], 
                             obscene = done_xgb[[3]], 
                             threat = done_xgb[[4]], 
                             insult = done_xgb[[5]], 
                             identity_hate = done_xgb[[6]])

# Random Forest

vocab <-  create_vocabulary(tok_train, ngram = c(1, 2))
pruned_vocab <- prune_vocabulary(vocab,
                                 term_count_min = 10,
                                 doc_proportion_max = 0.8)

vectorizer = vocab_vectorizer(pruned_vocab)

tfidf <- TfIdf$new(smooth_idf = TRUE, norm = "l2", sublinear_tf = TRUE)
dtm_train_tfidf <- create_dtm(tok_train, pruned_vocab) %>% 
  fit_transform(tfidf)


dtm_test_tfidf <- create_dtm(tok_test, pruned_vocab) %>% 
  transform(tfidf)

x <- dtm_train_tfidf
test_x <- dtm_test_tfidf

pr <- function(y_i, y, x){
  p <- Matrix::colSums(x[y == y_i, ])
  (p + 1)/(sum(y == y_i) + 1)
}

convert_sparse_matrix <- function(X){
  X.csc <- new("matrix.csc", ra = X@x,
               ja = X@i + 1L,
               ia = X@p + 1L,
               dimension = X@Dim)
  X.csr <- as.matrix.csr(X.csc)
  X.csr
}

fit_nblr <- function(y = "toxic", cost = 1/20){
  y <- train[[y]]
  r <- log(pr(1, y, x)/pr(0, y, x))
  x_nb <- x %*% Diagonal(x = r)
  x_nb <- convert_sparse_matrix(x_nb)
  
  fit <- LiblineaR(data = x_nb,
                   target = y,
                   type = 7,
                   cost = cost,
                   epsilon = 1e-4,
                   bias = 1)
  
  x_test <- test_x %*% Diagonal(x = r)
  x_test <- convert_sparse_matrix(x_test)
  pred <- predict(fit, x_test, proba = TRUE, decisionValues = TRUE)$probabilities[,"1"]
  return(pred)
}

subm <- data.frame(id = test$id)
costs <- c(0.12, 0.08, 0.08, 0.08, 0.08, 0.08)
category <- c("toxic", "severe_toxic", "obscene", "threat", 
              "insult", "identity_hate")
for(i in 1:length(category)){
  print(category[i])
  subm[, category[i]] <- fit_nblr(category[i], costs[i])
}

# calculating AUC XGB

auc_toxic <- auc(roc(test$toxic, prediction_xgb$preds))
auc_severe_toxic <- auc(roc(test$severe_toxic, prediction_xgb$preds.1))
auc_obscene <- auc(roc(test$obscene, prediction_xgb$preds.2))
auc_threat <- auc(roc(test$threat, prediction_xgb$preds.3))
auc_insult <- auc(roc(test$insult, prediction_xgb$preds.4))
auc_identity_hate <- auc(roc(test$identity_hate, prediction_xgb$preds.5))

auc_xgb <- mean(auc_toxic, auc_severe_toxic, auc_obscene, auc_threat, auc_insult, auc_identity_hate)

# calculating AUC LR

auc_toxic <- auc(roc(test$toxic, subm$toxic))
auc_severe_toxic <- auc(roc(test$severe_toxic, subm$severe_toxic))
auc_obscene <- auc(roc(test$obscene, subm$obscene))
auc_threat <- auc(roc(test$threat, subm$threat))
auc_insult <- auc(roc(test$insult, subm$insult))
auc_identity_hate <- auc(roc(test$identity_hate, subm$identity_hate))
                             
library(MLmetrics)

Accuracy(test$insult, subm$insult)
